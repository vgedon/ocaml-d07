let () =
	let doctor = new Doctor.doctor "Jon Pertwee" 22 (new People.people "Amelia \"Amy\" Pond") in
	doctor#talk;
	print_endline doctor#to_string;
	print_endline "===== Dr Who use its sonic screwdriver =====";
	doctor#use_sonic_screwdriver;
	print_endline "===== travel from 1950 to 2021 =====";
	print_endline (doctor#travel_in_time 1950 2021)#to_string;
	print_endline (doctor#kill_doctor)#to_string;
	print_endline ((doctor#kill_doctor)#back_to_life)#to_string