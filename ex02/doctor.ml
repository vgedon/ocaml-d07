class doctor name age sidekick =
  object (self)
    val _name:string = name
    val _hp:int = 100
    val _age:int = age
    val _sidekick = sidekick

    initializer print_endline "A tardis appears and a curious person leaves it"
    method to_string = _name ^ ", hp : " ^ string_of_int _hp ^ ", age : " ^ string_of_int _age ^ " followed by " ^ (_sidekick#to_string)
    method talk = print_endline "Hi! I'm the Doctor!"
    method use_sonic_screwdriver = print_endline "Whiiiiwhiiiwhiii Whiiiiwhiiiwhiii Whiiiiwhiiiwhiii"
    method private regenarate = {<_name = _name; _hp = 100; _age = _age; _sidekick = _sidekick>}
    method travel_in_time start arrival = begin
    print_endline "                 __/\\__
                |-+--+-|
                |-+--+-|
                |_|__|_|
       =========================
       =========================
    ===============================
+-------------------------------------+
| +---------------------------------+ |
| | POLICE     PUBLIC CALL      BOX | |
| +---------------------------------+ |
+-------------------------------------+
  | | +-----------+ +-----------+ | |
  | | | +-------+ | | +-------+ | | |
  | | | |  | |  | | | |  | |  | | | |
  | | | |--+-+--| | | |--+-+--| | | |
  | | | |--+-+--| | | |--+-+--| | | |
  | | | |  | |  | | | |  | |  | | | |
  | | | +-------+ | | +-------+ | | |
  | | |           | |           | | |
  | | | +-------+ | | +-------+ | | |
  | | | | ~~~~~ | | | |       | | | |
  | | | | ~~~~~ | | | |       | | | |
  | | | | ~~~~~ | | | |       | | | |
  | | | | ~~~~~ | | | |       | | | |
  | | | +-------+ | | +-------+ | | |
  | | |           | |           | | |
  | | | +-------+ | | +-------+ | | |
  | | | |       | | | |       | | | |
  | | | |       | | | |       | | | |
  | | | |       | | | |       | | | |
  | | | |       | | | |       | | | |
  | | | +-------+ | | +-------+ | | |
  | | |           | |           | | |
  | | | +-------+ | | +-------+ | | |
  | | | |       | | | |       | | | |
  | | | |       | | | |       | | | |
  | | | |       | | | |       | | | |
  | | | |       | | | |       | | | |
  | | | +-------+ | | +-------+ | | |
  | | +-----------+ +-----------+ | |
=======================================
=======================================";
    {<_age = (_age + abs(arrival - start)); _name = _name; _hp = _hp; _sidekick = _sidekick>}
      end
    method back_to_life = self#regenarate
    method kill_doctor = {<_name = _name; _hp = 0; _age = _age; _sidekick = _sidekick>}
  end