let () =
	let t = new People.people "William Hartnell" in
	t#to_string;
	t#talk;
	t#die;
	let u = new People.people "Patrick Troughton" in
	u#to_string;
	u#talk;
	u#die;